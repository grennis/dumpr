﻿
# What is it

Dumpr is an application that will recursively scan a directory structure and upload photo and video files to Flickr. The files are placed in a set with the name of the immediately containing folder. Uploads are recorded in a local SQL compact database so that the application can be re-run multiple times and only newly added or updated files will be uploaded.

# How to use it

Dump is a console command line application. Clone the repository and edit the app.config file to supply your flickr API key and secret. The first time you run the app you will be prompted by Flickr to authorize the application. Authorize it and then run the application again to start uploading.
