﻿using FlickrNet;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dumpr
{
    /// <summary>
    /// Flickr Uploader
    /// </summary>
    public class FlickrUploader
    {
        private Flickr _flickr;
        private FlickrUploaderConfig _config;
        private PicturesEntities _pe;

        /// <summary>
        /// Construct the flickr uploader helper
        /// </summary>
        /// <param name="config"></param>
        public FlickrUploader(FlickrUploaderConfig config)
        {
            _flickr = new Flickr(config.ApiKey, config.ApiSecret);
            _config = config;
        }

        /// <summary>
        /// Recursively search the given directory and upload all files matching the extension 
        /// </summary>
        /// <param name="dir"></param>
        public void Upload(string dir)
        {
            if (!ValidateAuthentication())
            {
                return;
            }

            using (_pe = new PicturesEntities())
            {
                //DeleteDatabase();

                RecurseDirectories(dir);
            }
        }

        private bool ValidateAuthentication()
        {
            Config config = ReadConfig();

            if (config != null)
            {
                if (!string.IsNullOrEmpty(config.AuthToken))
                {
                    _flickr.AuthToken = config.AuthToken;
                    return true;
                }

                if (!string.IsNullOrEmpty(config.Frob))
                {
                    config.AuthToken = _flickr.AuthGetToken(config.Frob).Token;
                    _flickr.AuthToken = config.AuthToken;
                    WriteConfig(config);
                    return true;
                }
            }

            Trace.TraceInformation("Authorize this application in the popup window and then run it again.");
            string frob = _flickr.AuthGetFrob();
            string flickrUrl = _flickr.AuthCalcUrl(frob, AuthLevel.Write);
            System.Diagnostics.Process.Start(flickrUrl);

            WriteConfig(new Config
            {
                Frob = frob
            });

            return false;
        }

        private Config ReadConfig()
        {
            using (var store = IsolatedStorageFile.GetUserStoreForAssembly())
            {
                try
                {
                    using (IsolatedStorageFileStream stream = store.OpenFile("config", FileMode.Open))
                    using (StreamReader sr = new StreamReader(stream))
                    {
                        string content = sr.ReadToEnd();
                        return JsonConvert.DeserializeObject<Config>(content);
                    }
                }
                catch
                {
                    return null;
                }
            }
        }

        private void WriteConfig(Config config)
        {
            using (var store = IsolatedStorageFile.GetUserStoreForAssembly())
            using (IsolatedStorageFileStream stream = store.OpenFile("config", FileMode.Create))
            using (StreamWriter sw = new StreamWriter(stream))
            {
                string content = JsonConvert.SerializeObject(config);
                sw.Write(content);
            }
        }

        private void RecurseDirectories(string dir)
        {
            // Skip picasa backups
            if (dir.Contains(".picasaoriginals"))
            {
                return;
            }

            string[] dirs = Directory.GetDirectories(dir).OrderBy(x => x).ToArray();

            foreach (string subdir in dirs)
            {
                RecurseDirectories(subdir);
            }

            // Gather all extensions across multiple enumerations
            IEnumerable<string> results = new List<string>();
            foreach (string ext in _config.FileExtensions.Split(','))
            {
                results = results.Union(Directory.EnumerateFiles(dir, "*." + ext));
            }

            if (results.Count() == 0)
            {
                return;
            }

            Trace.TraceInformation("Scanning {0} files in {1}", results.Count(), dir);
            string set = dir.Split('\\').Reverse().First();

            foreach (string file in results)
            {
                CheckFile(file, set);
            }
        }

        private void CheckFile(string file, string set)
        {
            // Check if file has been uploaded already and upload if not
            Media media = _pe.Medias.FirstOrDefault(x => x.LocalPath == file);
            DateTime lastWriteTime = File.GetLastWriteTimeUtc(file);

            if (ShouldUploadMedia(media, lastWriteTime))
            {
                ProcessFile(file, set, lastWriteTime, media);
            }
        }

        private bool ShouldUploadMedia(Media media, DateTime lastWriteTime)
        {
            // If it has never been uploaded
            if (media == null)
            {
                return true;
            }

            // If file has been modified since upload
            if (_config.UploadModified && media.FlickrID != null && media.FileDate != lastWriteTime)
            {
                return true;
            }

            // If file did not upload successfully
            if (_config.UploadFailed && media.FlickrID == null)
            {
                return true;
            }

            return false;
        }

        private void ProcessFile(string file, string setName, DateTime lastWritetime, Media media)
        {
            // Upload the file and store record in the database
            string flickrID = null;
            string error = null;
            Set set = _pe.Sets.FirstOrDefault(x => x.Name == setName);

            try
            {
                flickrID = UploadFile(file, setName, set == null ? null : set.FlickrID);
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }

            // If this is a retry of a failed file, update the existing record, otherwise create a new one
            if (media == null)
            {
                _pe.Medias.Add(new Media
                {
                    LocalPath = file,
                    FileDate = lastWritetime,
                    FlickrID = flickrID,
                    Error = error
                });
            }
            else
            {
                media.LocalPath = file;
                media.FileDate = lastWritetime;
                media.FlickrID = flickrID;
                media.Error = error;
            }

            _pe.SaveChanges();
        }

        private string UploadFile(string file, string setName, string setID)
        {
            string fileName = Path.GetFileName(file);

            Trace.TraceInformation("Uploading {0} to set {1}", fileName, setName);

            // Upload the file and create a set if needed (if not already created)
            using (FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read))
            {
                string photoID = _flickr.UploadPicture(fs, fileName, null, null, null, _config.VisibleToPublic, _config.VisibleToFamily, _config.VisibleToFriends, ContentType.Photo, SafetyLevel.Safe, HiddenFromSearch.None);

                if (setID == null)
                {
                    Trace.TraceInformation("Creating set {0}", setName);
                    Photoset set = _flickr.PhotosetsCreate(setName, photoID);

                    _pe.Sets.Add(new Set
                    {
                        Name = setName,
                        FlickrID = set.PhotosetId
                    });

                    _pe.SaveChanges();
                }
                else
                {
                    _flickr.PhotosetsAddPhoto(setID, photoID);
                }

                return photoID;
            }
        }

        private void DeleteDatabase()
        {
            // For development only / delete all media and sets in the local db (this will cause everything to be re-uploaded!)
            foreach (Media media in _pe.Medias)
            {
                _pe.Medias.Remove(media);
            }

            foreach (Set set in _pe.Sets)
            {
                _pe.Sets.Remove(set);
            }

            _pe.SaveChanges();
        }
    }
}
