﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dumpr
{
    class Program
    {
        static void Main(string[] args)
        {
            var uploader = new FlickrUploader(FlickrUploaderConfig.FromConfiguration());
            uploader.Upload(ConfigurationManager.AppSettings["MediaDirectory"]);

            Console.WriteLine("Done! Press any key to exit");
            Console.ReadLine();
        }
    }
}
