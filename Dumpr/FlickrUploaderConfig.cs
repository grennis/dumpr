﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dumpr
{
    public class FlickrUploaderConfig
    {
        public string ApiKey { get; set; }
        public string ApiSecret { get; set; }
        public string FileExtensions { get; set; }
        public bool UploadFailed { get; set; }
        public bool UploadModified { get; set; }
        public bool VisibleToPublic { get; set; }
        public bool VisibleToFamily { get; set; }
        public bool VisibleToFriends { get; set; }

        public static FlickrUploaderConfig FromConfiguration()
        {
            return new FlickrUploaderConfig
            {
                ApiKey = ConfigurationManager.AppSettings["FlickrApiKey"],
                ApiSecret = ConfigurationManager.AppSettings["FlickrApiSecret"],
                FileExtensions = ConfigurationManager.AppSettings["FileExtensions"],
                UploadFailed = bool.Parse(ConfigurationManager.AppSettings["UploadFailed"]),
                UploadModified = bool.Parse(ConfigurationManager.AppSettings["UploadModified"]),
                VisibleToPublic = bool.Parse(ConfigurationManager.AppSettings["VisibleToPublic"]),
                VisibleToFamily = bool.Parse(ConfigurationManager.AppSettings["VisibleToFamily"]),
                VisibleToFriends = bool.Parse(ConfigurationManager.AppSettings["VisibleToFriends"])
            };
        }
    }
}
